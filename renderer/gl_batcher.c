// gl_batcher.c: handles creating little batches of polys

#include "quakedef.h"

// these could go in a vertexbuffer/indexbuffer pair
#define MAX_BATCHED_SURFVERTEXES 512
#define MAX_BATCHED_SURFINDEXES 1028 //24288
#define VERTEXSIZE_SINGLE 5
const int sizeofvertex = VERTEXSIZE_SINGLE * sizeof(float);

static float r_batchedsurfvertexes[MAX_BATCHED_SURFVERTEXES * VERTEXSIZE_SINGLE];
static unsigned short r_batchedsurfindexes[MAX_BATCHED_SURFINDEXES];

static int r_numsurfvertexes = 0;
static int r_numsurfindexes = 0;

static qboolean multi = false;

void R_BeginBatchingSurfaces (int texcoordindex)
{
   //glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[texcoordindex]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   
   multi=false;
}

void R_BeginBatchingSurfacesMulti ()
{
   //glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glClientActiveTextureARB(GL_TEXTURE0);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[3]);

   glClientActiveTextureARB(GL_TEXTURE1);
   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[5]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;

   multi = true;
}

void R_BeginBatchingSurfacesQuad (int texcoordindex)
{  
   glEnable(GL_BLEND);
   glEnableClientState(GL_VERTEX_ARRAY);
   glVertexPointer(3, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[0]);

   glEnableClientState(GL_TEXTURE_COORD_ARRAY);
   glTexCoordPointer(2, GL_FLOAT, sizeofvertex, &r_batchedsurfvertexes[texcoordindex]);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
}

void R_EndBatchingSurfaces (void)
{
   if (r_numsurfvertexes && r_numsurfindexes)
   {
      glDrawElements(GL_TRIANGLES, r_numsurfindexes, GL_UNSIGNED_SHORT, r_batchedsurfindexes);
   }

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   memset(r_batchedsurfindexes, 0x0, MAX_BATCHED_SURFINDEXES);
   memset(r_batchedsurfvertexes, 0x0, MAX_BATCHED_SURFVERTEXES);

   multi = false;
}

void R_EndBatchingSurfacesQuads (void)
{
   if (r_numsurfvertexes && r_numsurfindexes)
   {
      glDrawArrays(GL_QUADS, 0, r_numsurfindexes);
   }
   glDisable(GL_BLEND);

   r_numsurfvertexes = 0;
   r_numsurfindexes = 0;
   sq_clr(r_batchedsurfvertexes, sizeof(r_batchedsurfvertexes));

   multi = false;
}

void R_BatchSurfaceQuadText(int x, int y, float frow, float fcol, float size)
{
   int i;
   unsigned short *ndx;

   if (r_numsurfvertexes + 4 >= MAX_BATCHED_SURFVERTEXES)
      R_EndBatchingSurfacesQuads();
   if (r_numsurfindexes + 4 >= MAX_BATCHED_SURFINDEXES)
      R_EndBatchingSurfacesQuads();

   //Vertex 1
   //Quad vertex
   r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 0] = x;
   r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 1] = y;
   r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 2] = 0;
   //Tex Coord
   r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 3] = fcol;
   r_batchedsurfvertexes[((r_numsurfvertexes + 0) * VERTEXSIZE_SINGLE) + 4] = frow;

   //Vertex 2
   //Quad vertex
   r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 0] = x+8;
   r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 1] = y;
   r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 2] = 0;
   //Tex Coord
   r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 3] = fcol+size;
   r_batchedsurfvertexes[((r_numsurfvertexes + 1) * VERTEXSIZE_SINGLE) + 4] = frow;

   //Vertex 3
   //Quad vertex
   r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 0] = x+8;
   r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 1] = y+8;
   r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 2] = 0;
   //Tex Coord
   r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 3] = fcol+size;
   r_batchedsurfvertexes[((r_numsurfvertexes + 2) * VERTEXSIZE_SINGLE) + 4] = frow+size;

   //Vertex 4
   //Quad vertex
   r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 0] = x;
   r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 1] = y+8;
   r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 2] = 0;
   //Tex Coord
   r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 3] = fcol;
   r_batchedsurfvertexes[((r_numsurfvertexes + 3) * VERTEXSIZE_SINGLE) + 4] = frow+size;

   r_numsurfvertexes += 4;
   r_numsurfindexes += 4;
}

void R_BatchSurface(glpoly_t *p)
{
   int i;
   int numindexes = (p->numverts - 2) * 3;
   unsigned short *ndx;

   if (r_numsurfvertexes + p->numverts >= MAX_BATCHED_SURFVERTEXES)
      R_EndBatchingSurfaces();
   if (r_numsurfindexes + numindexes >= MAX_BATCHED_SURFINDEXES)
      R_EndBatchingSurfaces();

   memcpy(&r_batchedsurfvertexes[r_numsurfvertexes * VERTEXSIZE], p->verts, p->numverts * sizeofvertex);
   ndx = &r_batchedsurfindexes[r_numsurfindexes];

   for (i = 2; i < p->numverts; i++, ndx += 3)
   {
      ndx[0] = r_numsurfvertexes;
      ndx[1] = r_numsurfvertexes + i - 1;
      ndx[2] = r_numsurfvertexes + i;
   }
   //printf("%s: total verts %d\n",__func__, i);
   r_numsurfvertexes += p->numverts;
   r_numsurfindexes += numindexes;
}