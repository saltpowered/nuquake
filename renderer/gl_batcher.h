/*
 * File: gl_batcher.h
 * Project: renderer
 * File Created: Saturday, 23rd March 2019 5:42:06 pm
 * Author: Hayden Kowalchuk (hayden@hkowsoftware.com)
 * -----
 * Copyright (c) 2019 Hayden Kowalchuk
 */

#ifndef __GL_BATCHER__
#define __GL_BATCHER__

#include "gl_model.h"

void R_BeginBatchingSurfaces(int texcoordindex);
void R_BeginBatchingSurfacesMulti();
void R_BeginBatchingSurfacesQuad (int texcoordindex);

void R_EndBatchingSurfaces(void);
void R_EndBatchingSurfacesQuads(void);

void R_BatchSurface(glpoly_t *p);
void R_BatchSurfaceQuadText(int x, int y, float frow, float fcol, float size);

#endif /* __GL_BATCHER__ */